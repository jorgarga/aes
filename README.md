# AES

Proof of concept for encrypting/decrypting files with Rust using [AEAD](https://en.wikipedia.org/wiki/Authenticated_encryption).
I started the project at the beginning of December 2021, sadly I had to stop because I did not understand a couple of things.
Later on -trying to find how to continue- I found what I was looking for very clearly explained on [Sylvain Kerkour's](https://kerkour.com/rust-file-encryption-chacha20poly1305-argon2) web page.

I used his code to fill in the gaps of my code.


