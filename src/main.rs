extern crate argon2;
extern crate base64;
extern crate chacha20poly1305;
extern crate rpassword;
extern crate rand;
extern crate anyhow;
extern crate zeroize;

use zeroize::Zeroize;
use anyhow::{anyhow, Error};
use chacha20poly1305::{
    aead::{stream, NewAead},
    XChaCha20Poly1305,
};
use std::env;
use std::{fs::File, io::{Read, Write}};
use std::process;
use std::str;
use argon2::Config;
use rand::{rngs::OsRng,RngCore};

const BUFFER_LEN: usize = 5000;
const SALT_SIZE: usize = 32;
const NONCE_SIZE: usize = 19;

fn derive_key(password: &str, salt: &[u8], config: &Config) -> Result<Vec<u8>, Error> {
    let password_hash = argon2::hash_raw(password.as_bytes(), salt, config);
    Ok(password_hash.unwrap())
}

fn argon2_config<'a>() -> argon2::Config<'a> {
    argon2::Config {
        variant: argon2::Variant::Argon2id,
        hash_length: 32,
        lanes: 8,
        mem_cost: 16 * 1024,
        time_cost: 8,
        ..Default::default()
    }
}

fn create_random_nonce() -> [u8; NONCE_SIZE] {
    let mut nonce = [0u8; NONCE_SIZE];
    OsRng.fill_bytes(&mut nonce);
    nonce
}

fn create_random_salt() -> [u8; SALT_SIZE] {
    let mut salt = [0u8; SALT_SIZE];
    OsRng.fill_bytes(&mut salt);
    salt
}

fn print_help() {
    println!("Usage:");
    println!("aescrypt enc path/to/file  # encrypts file");
    println!("aescrypt dec path/to/file  # decrypts file");
}


fn main() -> Result<(), anyhow::Error> {
    let args: Vec<String> = env::args().collect();

    if 3 != args.len() {
        print_help();
        process::exit(1);
    }

    let mode = args[1].as_str();
    let file: String = args[2].parse::<String>().unwrap();

    let mut password = rpassword::read_password_from_tty(Some("Password: ")).unwrap();

    match mode {
        "enc" => {
            println!("Encrypting...");
            encrypt_large_file(
                file.as_str(),
                &(file.clone() + ".enc"),
                &password
            )?;
        }
        "dec" => {
            println!("Decrypting...");
            decrypt_large_file(
                file.as_str(),
                file.clone().strip_suffix(".enc").expect("Missing '.enc' in file name."),
                &password,
            )?;
        }
        _ => {print_help()}
    }

    password.zeroize();

    Ok(())
}

fn encrypt_large_file(
    clear_file_path: &str,
    encrypted_file_path: &str,
    password: &str
) -> Result<(), anyhow::Error> {
    let argon2_config = argon2_config();
    let mut salt = create_random_salt();
    let mut key = derive_key(password, &salt, &argon2_config).unwrap();
    let mut nonce = create_random_nonce();

    let aead = XChaCha20Poly1305::new(key.as_slice().into());
    let mut stream_encryptor = stream::EncryptorBE32::from_aead(aead, nonce.as_ref().into());

    let mut buffer = [0u8; BUFFER_LEN];

    let mut source_file = File::open(clear_file_path)?;
    let mut dist_file = File::create(encrypted_file_path)?;


    let written = dist_file.write(&salt);
    assert_eq!(written.unwrap(), salt.len());

    let written = dist_file.write(&nonce);
    assert_eq!(written.unwrap(), nonce.len());

    loop {
        let read_count = source_file.read(&mut buffer)?;

        if read_count == BUFFER_LEN {
            let ciphertext = stream_encryptor
                .encrypt_next(buffer.as_ref())
                .map_err(|err| anyhow!("Encrypting large file: {}", err))?;
            dist_file.write_all(&ciphertext)?;
        } else {
            let ciphertext = stream_encryptor
                .encrypt_last(&buffer[..read_count])
                .map_err(|err| anyhow!("Encrypting large file: {}", err))?;
            dist_file.write_all(&ciphertext)?;
            break;
        }
    }

    salt.zeroize();
    nonce.zeroize();
    key.zeroize();

    Ok(())
}

fn decrypt_large_file(
    encrypted_file_path: &str,
    clear_file_path: &str,
    password: &str,
) -> Result<(), anyhow::Error> {
    let mut encrypted_file = File::open(encrypted_file_path)?;
    let mut dist_file = File::create(clear_file_path)?;

    let mut salt = [0u8; SALT_SIZE];
    let read = encrypted_file.read(&mut salt);
    assert_eq!(read.unwrap(), salt.len());

    let mut nonce = [0u8; NONCE_SIZE];
    let read = encrypted_file.read(&mut nonce);
    assert_eq!(read.unwrap(), nonce.len());

    let argon2_config = argon2_config();
    let mut key = derive_key(password, &salt, &argon2_config).unwrap();

    let aead = XChaCha20Poly1305::new(key.as_slice().into());
    let mut stream_decryptor = stream::DecryptorBE32::from_aead(aead, nonce.as_ref().into());

    const READ_BUFFER: usize = BUFFER_LEN + 16;
    let mut buffer = [0u8; READ_BUFFER];

    loop {
        let read_count = encrypted_file.read(&mut buffer)?;

        if read_count == READ_BUFFER {
            let plaintext = stream_decryptor
                .decrypt_next(buffer.as_ref())
                .map_err(|err| anyhow!("Decrypting large file: {}", err))?;
            dist_file.write_all(&plaintext)?;
        } else {
            let plaintext = stream_decryptor
                .decrypt_last(&buffer[..read_count])
                .map_err(|err| anyhow!("Decrypting large file: {}", err))?;
            dist_file.write_all(&plaintext)?;
            break;
        }
    }

    salt.zeroize();
    nonce.zeroize();
    key.zeroize();

    Ok(())
}
